package cat.escolapia.damviod.pmdm.snake;


import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;

public class GameScene extends Screen {

    enum GameState {
        Starting,
        Running,
        Paused,
        GameOver,
        EndScreenWin
    }

    GameState state = GameState.Starting;
    World world;

    int xi = 0;
    int xf = 0;
    int yi = 0;
    int yf = 0;
    int distx = 0;
    int disty = 0;

    public GameScene(Game game){
        super(game); world = new World();
    }

    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        if(state == GameState.Starting)
            updateStarting(touchEvents);
        if(state == GameState.Running)
            updateRunning(touchEvents, deltaTime);
        if(state == GameState.Paused)
            updatePaused(touchEvents);
        if(state == GameState.GameOver)
            updateGameOver(touchEvents);
        if(state == GameState.EndScreenWin)
            updateEndScreenWin(touchEvents);
    }
    private void updateStarting(List<TouchEvent> touchEvents) {
        if(touchEvents.size() > 0) state = GameState.Running;
    }
    private void getTouchInitial(TouchEvent event){
        xi = event.x;
        yi = event.y;
    }

    private void getTouchFinal(TouchEvent event){
        xf = event.x;
        yf = event.y;
        distx = xf-xi;
        disty = yf-yi;
    }
    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_DOWN) {
                getTouchInitial(event);
            }

            if(event.type == TouchEvent.TOUCH_UP) {
                getTouchFinal(event);


                if(distx < -30)
                {
                    world.king.Left();
                }
                else if(distx > 30)
                {
                    world.king.Right();
                }
                else if(disty < -30)
                {
                    world.king.Up();
                }
                else if(disty > 30)
                {
                    world.king.Down();
                }
            }
        }
        world.update(deltaTime);

        if(world.gameOver) {

            state = GameState.GameOver;
        }
        if(world.EndGameBool){
            state = GameState.EndScreenWin;
        }

    }
    private void updatePaused(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {

                state = GameState.Running;
                return;
            }
            }
        }


    private void updateGameOver(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 0 && event.x <= 320 &&
                        event.y >= 0 && event.y <= 480) {

                    game.setScreen(new MainMenu(game));
                    return;
                }
            }
        }
    }

    private void updateEndScreenWin(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 0 && event.x <= 320 &&
                        event.y >= 0 && event.y <= 480) {

                    game.setScreen(new MainMenu(game));
                    return;
                }
            }
        }
    }
    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();


        if(world.countKing < 3)g.drawPixmap(Assets.Background_2, 0, 0);
        if(world.countKing == 3)g.drawPixmap(Assets.Background_3, 0, 0);

        drawWorld(world);
        if(state == GameState.Starting)
            drawStartingUI();
        if(state == GameState.Running)
            drawRunningUI();
        if(state == GameState.Paused)
            drawPausedUI();
        if(state == GameState.GameOver)
            drawGameOverUI();
        if(state == GameState.EndScreenWin)
            drawEndScreenWinUI();

        drawText(g, Integer.toString(world.score), g.getWidth() / 2 - Integer.toString(world.score).length() * 20 / 2, g.getHeight() - 42);

    }

    private void drawWorld(World world) {
        Graphics g = game.getGraphics();


        King king = world.king;
        Exit Exit_1 = world.Exit_1;
        Claus[] Claus = world.Claus;

        Pixmap Exit_1Pixmap = null;
        Exit_1Pixmap = Assets.Exit_1;
        Pixmap ClausPixmap = null;
        Pixmap kingPixmap = null;
        if(king.type == 1 || king.type ==4){
            kingPixmap = Assets.Melchor;
        }
        if(king.type == 2){
            kingPixmap = Assets.Gaspar;
        }
        if(king.type == 3){
            kingPixmap = Assets.Baltasar;
        }

        ClausPixmap = Assets.Claus;


        for(int i = 0; i<Claus.length; i++){
            int xs = Claus[i].x * 32;
            int ys = Claus[i].y * 32;
            g.drawPixmap(ClausPixmap, xs , ys );
        }
        int x = king.x * 32;
        int y = king.y * 32;
        g.drawPixmap(kingPixmap, x , y );

        int xd1 = Exit_1.x * 32;
        int yd1 = Exit_1.y * 32;

        g.drawPixmap(Exit_1Pixmap, xd1 , yd1 );



    }

    private void drawStartingUI() {
        Graphics g = game.getGraphics();


    }

    private void drawRunningUI() {
        Graphics g = game.getGraphics();

    }

    private void drawPausedUI() {
        Graphics g = game.getGraphics();

    }

    private void drawGameOverUI() {

        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.GameOver, 35, 100);


    }

    private void drawEndScreenWinUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.Victory_Screen, 1, 100);


    }

    public void drawText(Graphics g, String line, int x, int y) {
        int len = line.length();
        for (int i = 0; i < len; i++) {
            char character = line.charAt(i);

            if (character == ' ') {
                x += 20;
                continue;
            }

            int srcX = 0;
            int srcWidth = 0;
            if (character == '.') {
                srcX = 200;
                srcWidth = 10;
            } else {
                srcX = (character - '0') * 20;
                srcWidth = 20;
            }


            x += srcWidth;
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
