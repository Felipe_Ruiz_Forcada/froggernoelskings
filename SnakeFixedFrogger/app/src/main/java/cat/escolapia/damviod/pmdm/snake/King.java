package cat.escolapia.damviod.pmdm.snake;



public class King {
    public int x, y;
    int type;
    public King(int x, int y, int type)
    {
        this.x = x;
        this.y = y;
        this.type = type;
    }

    public void Right()
    {
        x +=1;
    }
    public void Left()
    {
        x -=1;
    }
    public void Up()
    {
        y -=1;
    }
    public void Down()
    {
        y +=1;
    }



}
