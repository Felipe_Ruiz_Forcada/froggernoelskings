package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Pixmap;
        import cat.escolapia.damviod.pmdm.framework.Music;
        import cat.escolapia.damviod.pmdm.framework.Sound;


public class Assets {
    public static Pixmap Background;
    public static Pixmap Background_2;
    public static Pixmap Background_3;
    public static Pixmap Melchor;
    public static Pixmap Gaspar;
    public static Pixmap Baltasar;
    public static Pixmap Exit_1;
    public static Pixmap Exit_2;
    public static Pixmap Exit_3;
    public static Pixmap Start_Point;
    public static Pixmap Claus;
    public static Pixmap GameOver;
    public static Pixmap Victory_Screen;

}