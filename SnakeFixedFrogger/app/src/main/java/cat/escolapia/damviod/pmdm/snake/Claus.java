package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Screen;

public class Claus {
    public int x, y;
    public int direction;

    public Claus(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public void InfinityMove()
    {
        if (this.x <= -1) {
            this.x = 9;
        }
        if (this.x >= 10) {
            this.x = 0;
        }

    }

    public void Move() {
        this.x += 1;

    }
}
