package cat.escolapia.damviod.pmdm.snake;
import java.util.Random;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 15;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.02f;

    public King king;


    public Exit Exit_1;
    public Claus[] Claus;
    public boolean gameOver = false;
    public boolean EndGameBool = false;
    public int score = 0;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;
    int countKing = 1;

    public World() {

        Claus = new Claus[13];
        for(int i = 0; i<Claus.length; i++){
            int ClausX = random.nextInt(WORLD_WIDTH);
            Claus[i] = new Claus(ClausX,1+i);
        }
        int ExitXrandom = random.nextInt(WORLD_WIDTH);
        king = new King(4,14,1);
        Exit_1 = new Exit(ExitXrandom,0);


    }


    public void update(float deltaTime) {
        if (gameOver) return;

        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            for(int i = 0; i<Claus.length; i++) {
                Claus[i].Move();
                Claus[i].InfinityMove();
                if(Claus[i].x == king.x && Claus[i].y == king.y){

                    gameOver = true;





                }
            }


            if(Exit_1.x == king.x && Exit_1.y == king.y){
                for(int i = 0; i<Claus.length; i++){
                    int ClausX = random.nextInt(WORLD_WIDTH);
                    Claus[i] = new Claus(ClausX,1+i);
                }
                int ExitXrandom = random.nextInt(WORLD_WIDTH);
                Exit_1 = new Exit(ExitXrandom,0);
                king = new King(5,14,countKing+1);
                countKing++;
            }
            if(countKing >= 4){
                EndGameBool = true;
            }

        }
    }

}
