package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;

public class LoadingScreen extends Screen {

    public LoadingScreen(Game game) {
        super(game);
    }
    public void update(float deltaTime){
        Graphics g = game.getGraphics();
        Assets.Background = g.newPixmap("Background_1.png", Graphics.PixmapFormat.RGB565);
        Assets.Background_2 = g.newPixmap("Background_2.png", Graphics.PixmapFormat.RGB565);
        Assets.Background_3 = g.newPixmap("Background_3.png", Graphics.PixmapFormat.RGB565);
        Assets.Melchor = g.newPixmap("King.png", Graphics.PixmapFormat.RGB565);
        Assets.Gaspar = g.newPixmap("King_2.png", Graphics.PixmapFormat.RGB565);
        Assets.Baltasar = g.newPixmap("King_3.png", Graphics.PixmapFormat.RGB565);
        Assets.Start_Point = g.newPixmap("Icon_Start_But.png", Graphics.PixmapFormat.RGB565);
        Assets.Exit_1 = g.newPixmap("Exit_1.png", Graphics.PixmapFormat.RGB565);
        Assets.Exit_2 = g.newPixmap("Exit_2.png", Graphics.PixmapFormat.RGB565);
        Assets.Exit_3 = g.newPixmap("Exit_3.png", Graphics.PixmapFormat.RGB565);
        Assets.Claus = g.newPixmap("Claus.png", Graphics.PixmapFormat.RGB565);
        Assets.GameOver = g.newPixmap("GameOver_Screen_msg.png", Graphics.PixmapFormat.RGB565);
        Assets.Victory_Screen = g.newPixmap("End_Game_msg.png", Graphics.PixmapFormat.RGB565);
        game.setScreen(new MainMenu(game));
    }
    public void render(float deltaTime){

    }
    public void pause() {

    }
    public void resume(){

    }
    public void dispose(){

    }
}

